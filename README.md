BYMatt
=========
Simulador para la actividad final del período 2 de 7 año de la materia Instalación Mantenimiento y Reparación de Sistemas Computacionales.
Se utiliza para su desarollo y pruebas:

- HTML.
- CSS.
- Javascript.
- PHP.
- MySql(MariaDB).

Temas que se aplican
====================

- Manejo correcto de etiquetas HTML5.
- Uso de reglas CSS3 e iconos de Fontawesome.
- Lógica de programación en PHP y buenas prácticas.
- Creación de bases de datos relacionales en MySql.
- Comprensión y aplicación de consultas sql (SELECT, INSERT, UPDATE y DELETE).
- Diseño de formularios con conexión a bases de datos.

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- http://www.mbcorp.com.ar
