

// Una vez cargada la pagina entera
document.addEventListener("DOMContentLoaded", () => {
	// Recarga de datos al pasar 1 segundo
	id = setInterval(loadByma, 1000)
})


// Carga el JSON
async function loadByma(){
	const response = await fetch('byma.php')
	const data = await response.json()

	showByma(data)
}


// Muestra los datos en la tabla
function showByma(filas){

	// Limpia la tabla
	clearFilas()

	// Recorre las filas del Json y las convierte en filas de una tabla
	for (var i = 0; i < filas.length; i++) {
		newFila(filas[i], i)
	}
}

// Limpia la tabla
function clearFilas(){
	var panel = document.querySelector("#panel")
	var filas = panel.querySelectorAll(".fila")

	// Borramos fila a fila
	for (var i = 0; i < filas.length ; i++) {
		panel.removeChild(filas[i])
	}
}

// Crea una nueva fila con la información especificada
function newFila(info, orden){
	var panel = document.querySelector("#panel")
	var temp = document.querySelector("#fila")
	var clon = temp.content.cloneNode(true)
	var fila = clon.querySelector(".fila")

	// Si no es el primer item (es una fila con datos)
	if (orden != 0){

		fila.querySelector(".ticker").className += " ticker-estilo"

		// Si el la variacion es positiva (no tiene el -)
		if (info.Var.indexOf("-")==-1){
			fila.querySelector(".var").className += " var-color_verde"
		}else{ // es negativa
			fila.querySelector(".var").className += " var-color_rojo"
		}

		fila.querySelector(".pcompra").className += " pcv-estilo cpcompra-fondo_verde"
		fila.querySelector(".ccompra").className += " cpcompra-fondo_verde"
		fila.querySelector(".pventa").className += " pcv-estilo cpventa-fondo_rojo"
		fila.querySelector(".cventa").className += " cpventa-fondo_rojo"

		// Si la fila es par le damos color gris
		if (orden % 2 == 0){
			fila.className += " fila-fondo_gris"
		}else{ // es impar color blanco
			fila.className += " fila-fondo_white"
		}

	}else{ // Es el primer item (Titulos de tabla)
		fila.className += " fila-fondo_titulo";
	}

	fila.querySelector(".ticker").innerHTML = info.Ticker
	fila.querySelector(".precio").innerHTML = info.Precio
	
	fila.querySelector(".var").innerHTML = info.Var
	fila.querySelector(".precioa").innerHTML = info.PrecioA
	fila.querySelector(".ccompra").innerHTML = info.CCompra
	fila.querySelector(".pcompra").innerHTML = info.PCompra
	fila.querySelector(".pventa").innerHTML = info.PVenta
	fila.querySelector(".cventa").innerHTML = info.CVenta

	fila.querySelector(".volnom").innerHTML = info.VolNom
	fila.querySelector(".monto").innerHTML = info.Monto
	fila.querySelector(".oper").innerHTML = info.Oper
	fila.querySelector(".hora").innerHTML = info.Hora

	// Agregamos el clon
	panel.appendChild(clon)

}
