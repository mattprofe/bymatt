<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
		include '../tpl/head-style.php';
	 ?>
	
	<link rel="stylesheet" href="byma.css">

	<title>BYMatt</title>
</head>
<body>

	<?php
		include '../tpl/header.php'; 
	 ?>

	
	<div id="menu">

		<label for="btn-pgeneral" class="boton">
			<input type="checkbox" id="btn-pgeneral" value="" name="btn-pgeneral" style="visibility: hidden;">
			Panel General
		</label>

		<label for="btn-cedears" class="boton">
			<input type="checkbox" id="btn-cedears" value="" name="btn-cedears" style="visibility: hidden;">
			Cedears
		</label>
		
	</div>

	<div id="panel">
		
		<template id="fila">

			<div class="fila">

				<div class="ticker columna">
				</div>

				<div class="precio columna">
				</div>

				<div class="var columna">
				</div>

				<div class="precioa columna">
				</div>

				<div class="ccompra columna">
				</div>

				<div class="pcompra columna">
				</div>

				<div class="pventa columna">
				</div>

				<div class="cventa columna">
				</div>

				<div class="volnom columna">
				</div>

				<div class="monto columna">
				</div>

				<div class="oper columna">
				</div>

				<div class="hora columna">
				</div>
				
			</div>

		</template>

	</div>
	
	<table>

		<template id="fila">
			<tr class="fila">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</template>

	</table>

	<?php 
		include '../tpl/pie.php';
	 ?>

	<script type="text/javascript" src="byma.js"></script>

</body>
</html>
