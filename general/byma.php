<?php

	// Arreglo con los nombres de las acciones
	$accion = array("ALUA","BBAR","BMA","BYMA","CEPU","COME","CRES","CVH","EDN","GGAL","HARG","LOMA","MIRG","PAMP","SUPV","TECO2","TGNO4","TGSU2","TRAN","TXAR","VALO","YPFD");

	// Retorna un array con los valores de la acción especificada por su indice
	function getDatosAccion($id){
		global $accion;

		$pos = array("Ticker" => '<a class="ticker-a" href="../acciones/?nombre='.$accion[$id].'">'.$accion[$id].'</a>',
				"Precio" => rand(10, 100).','.rand(100, 999),
				"Var" => rand(-50, 50).','.rand(0, 99).'%',
				"PrecioA" => rand(10, 100).','.rand(100, 999),
				"CCompra" => rand(10, 100).'.'.rand(100, 999),
				"PCompra" => rand(10, 100).','.rand(100, 999),
				"PVenta" => rand(10, 100).','.rand(100, 999),
				"CVenta" => rand(10, 100).'.'.rand(100, 999),
				"VolNom" => rand(10, 999).'K',
				"Monto" => '$'.rand(10, 100).'.'.rand(100, 999).'K',
				"Oper" => strval(rand(10, 500)),
				"Hora" => date('h:i:s'));

		return $pos;
	}

	// Arreglo que contendra los datos para crear el Json
	$byma = array();

	// Insertamos los titulos en la primer fila
	array_push($byma ,array("Ticker" => "Ticker",
				"Precio" => "Precio",
				"Var" => "Var.%",
				"PrecioA" => "Precio. A.",
				"CCompra" => "C. Compra",
				"PCompra" => "P. Compra",
				"PVenta" => "P. Venta",
				"CVenta" => "C. Venta",
				"VolNom" => "Vol. Nom.",
				"Monto" => "Monto",
				"Oper" => "Oper",
				"Hora" => "Hora"));

	// Insertamos todas las acciones con sus datos en el arreglo
	for ($i=0; $i < count($accion); $i++) { 
		array_push($byma, getDatosAccion($i));
	}

	// para que el retorno de las funciones sea un Json
	header('Content-Type: application/json');

	// Imprime el Json en la página
	echo json_encode($byma);
 ?>
