<?php
	// Captura de las siglas de la acción
	if(isset($_GET["nombre"])){
		$nombre = $_GET["nombre"];
	}else{
		$nombre = "null";
	}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
		include '../tpl/head-style.php';
	 ?>
	
	<link rel="stylesheet" href="acciones.css">

	<title>ByMatt <?php echo $nombre; ?></title>
</head>

<body>

	<?php
		include '../tpl/header.php'; 
	 ?>


	<div id="menu">

		<div class="titulo-accion">
			<div><img class="logo" src="" alt=""></div>
			<div class="accion"><?php echo $nombre; ?><span class="nombre"></span></div>
			<div style="text-align: right; padding-right: 1em;"><a href="../general/">Volver</a></div>
		</div>
	
	</div>	

	<div class="cuerpo">
		<div class="foro">
			<div class="caja-titulo">
				<h2>Foro</h2>Últimos mensajes
			</div>
			
			<div class="filas">
				<div class="fila fila-fondo_gris">
					<div class="re">
						<b>Re: Grupo XXXXX</b>
					</div>
					
					<span class="user">
						<b>UsuarioXXXX</b>
					</span>
				
					<span class="tiempo">
						| 17:42 | 28 de enero de 2021
					</span>
					
					<br><br>
				
					<div class="mensaje">
						238.000 papeles operados a 20 minutos para el cierre, increíble el nulo interés, nada.
					</div>
				</div>

				<div class="fila fila-fondo_blanco">
					<div class="re">
						<b>Re: Grupo XXXXX</b>
					</div>
					
					<span class="user">
						<b>UsuarioXXXX</b>
					</span>
				
					<span class="tiempo">
						| 17:42 | 28 de enero de 2021
					</span>
					
					<br><br>
				
					<div class="mensaje">
						Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Eius iure dicta, dolores omnis maiores voluptatum illo inventore facere veritatis error fugit blanditiis, ex mollitia beatae expedita pariatur, incidunt. Sint, laudantium!
						Blanditiis, quidem culpa alias voluptatum doloremque incidunt et, laboriosam non quae itaque assumenda esse sed quasi sequi commodi voluptate corrupti quia totam doloribus repudiandae libero rerum. Vitae totam illo facere.
						A provident earum dicta, et cum, debitis eligendi necessitatibus fugiat voluptatibus esse similique odit molestiae assumenda quia. Nemo, ratione laborum impedit dolor quo, fugit, nam, voluptatem voluptatum alias vero minima.
						Dolore quibusdam recusandae veniam molestias aliquid exercitationem in facilis labore optio, deleniti nobis, deserunt quos, amet quod aut ex culpa maxime! Sint fugit illum pariatur voluptates tempore quas consectetur libero.
					</div>
				</div>

				<div class="fila fila-fondo_gris">
					<div class="re">
						<b>Re: Grupo XXXXX</b>
					</div>
					
					<span class="user">
						<b>UsuarioXXXX</b>
					</span>
				
					<span class="tiempo">
						| 17:42 | 28 de enero de 2021
					</span>
					
					<br><br>
				
					<div class="mensaje">
						238.000 papeles operados a 20 minutos para el cierre, increíble el nulo interés, nada.
					</div>
				</div>	
			</div>
		</div>

		<div class="perfil">
			<div class="caja-titulo">
				<h2>Perfil</h2>
			</div>
			<div class="detalle">
			</div>
		</div>
	</div>

	<?php 
		include '../tpl/pie.php';
	 ?>

	<script type="text/javascript" src="acciones.js"></script>
	
</body>
</html>