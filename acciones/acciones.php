<?php 

	// Accedemos al archivo que contiene los datos de las acciones
	$arch = fopen("acciones_perfil.txt","r");

	$fila ="";

	// Cargamos el archivo en una variable linea por linea
	while (!feof($arch)) {
		$fila .= fgets($arch);
	}

	// Cierre del archivo
	fclose($arch);

	// Conversión a un vector
	$f = explode("|", $fila);

	// Creamos el arreglo que contendra lo que luego será el JSON
	$lista = array();

	// Recorremos el vector y lo pasamos al arreglo
	for ($i=0; $i < count($f)-1; $i++) { 
		
		// Guardamos el nombre para mejor legibilidad
		$nombre = $f[$i];
		// Extraemos solo las siglas y le quitamos cualquier otro caracter
		$nom = str_replace(chr( 194 ) . chr( 160 ), "",explode(" ", $nombre)[0]);

		// Cantidad de caracteres para mostrar al ingresar
		$limit = 300;

		// Limitamos los caracteres y ponemos un boton de ver mas
		$minimo = '<span id="minimo">'.mb_substr($f[$i+2],0,$limit,"UTF-8").'<a href="#desplegar" onclick="vermas(\'mas\');" id="mas">... [leer más]</a></span>';

		// Contenido completo oculto hasta que se presiona el botón
		$maximo = '<span id="maximo" style="display:none;">'.$f[$i+2].'</span>';

		//mb_substr($f[$i+2],$limit,NULL,"UTF-8")

		// Insertamos el arreglo dentro de una posicion del otro arreglo
		array_push($lista ,array("siglas" => $nom,
				"nombre" => str_replace($nom, "", $nombre),
				"imgUrl" => $f[$i+1],
				"detalle" => $minimo.$maximo));

		// Forzamos el incremento del for
		$i = $i+2;
	}
	


	// para que el retorno de las funciones sea un Json
	header('Content-Type: application/json');

	// Imprime el JSON en la página
	echo json_encode($lista);

 ?>