// Una vez cargada la pagina entera
document.addEventListener("DOMContentLoaded", () => {

	// Cargamos las acciones
	loadAcciones()
})


// Carga el JSON con los datos de las acciones
async function loadAcciones(){
	const response = await fetch('acciones.php')
	const data = await response.json()

	// Buscamos la acción en el listado
	findAccion(data)
}


// Busca la acción
function findAccion(filas){

	// Guarda el nombre de la acción a mostrar su detalle
	const accion = document.querySelector(".accion").innerText

	// Recorremos las filas
	for (var i = 0; i < filas.length; i++) {

		// Si coincide el nombre de la acción con el nombre de la fila
		if(accion == filas[i].siglas){

			// Colocamos el logo, el nombre completo y el detalle
			document.querySelector(".logo").src = filas[i].imgUrl
			document.querySelector(".nombre").innerHTML = filas[i].nombre
			document.querySelector(".detalle").innerHTML = filas[i].detalle

			// Dejamos de buscar
			break;
		}
	}
}

function vermas(id){
	if(id=="mas"){
		document.getElementById("minimo").style.display = "none";
		document.getElementById("maximo").style.display="block";   
		//document.getElementById("mas").style.display="none"; 
	}
}