<?php 
	// Configuracion para el acceso a la DB
	define("HOST","localhost");
	define("USER","bolsa");
	define("PASS","bolsa2020");
	define("DB","bolsa");
	
	// Si hay datospl en el vector de POST
	if(isset($_POST["datospl"])){
		
		// Capturamos datospl
		$vaca = $_POST["datospl"];

		// si hay que tomar una muestra nueva
		if(isset($_GET["muestra"])){
			
			// creamos un archivo para la muestra
			$arch = fopen("cadena.txt", "w");

			// guardamos la muestra
			fwrite($arch, $vaca);

			// cierro el archivo
			fclose($arch);

			// damos aviso y cortamos la ejecutcion
			echo "<h1>Muestra Tomada</h1>";
			exit();
		}

	}else{ // No ingreso datospl por POST entonces usamos una muestra manual


		$vaca = "['ALUA', '48,350', '0,72%', '48,000', '3.112', '48,350', '48,450', '1.436', '275K', '$13.243K', '473', '13:54:58', 'BBAR', '158,100', '-0,81%', '159,400', '10', '158,050', '158,100', '981', '82K', '$13.001K', '136', '13:53:56', 'BMA', '234,800', '0,38%', '233,900', '296', '234,800', '234,850', '72', '112K', '$26.561K', '272', '13:54:32', 'BYMA', '600,000', '-0,57%', '603,500', '308', '600,000', '602,000', '14', '22K', '$13.411K', '115', '13:51:58', 'CEPU', '37,200', '-0,13%', '37,250', '100', '37,100', '37,200', '72', '110K', '$4.110K', '202', '13:54:14', 'COME', '2,550', '-0,39%', '2,560', '11.452', '2,540', '2,550', '93.355', '531K', '$1.356K', '190', '13:55:01', 'CRES', '56,700', '-1,13%', '57,350', '578', '56,600', '56,700', '2.102', '44K', '$2.527K', '152', '13:53:44', 'CVH', '464,000', '1,42%', '457,500', '3', '460,000', '464,000', '211', '9K', '$4.358K', '253', '13:53:39', 'EDN', '26,000', '-0,19%', '26,050', '39', '25,850', '26,000', '5.794', '83K', '$2.164K', '176', '13:54:21', 'GGAL', '128,150', '-0,07%', '128,250', '65', '128,150', '128,300', '13.699', '845K', '$109.054K', '877', '13:55:02', 'HARG', '125,250', '1,00%', '124,000', '800', '125,500', '126,000', '276', '32K', '$4.079K', '160', '13:54:33', 'LOMA', '173,000', '2,00%', '169,600', '195', '173,000', '173,450', '374', '179K', '$30.956K', '340', '13:54:58', 'MIRG', '1.455,000', '2,64%', '1.417,500', '129', '1.450,000', '1.455,000', '252', '9K', '$13.069K', '288', '13:53:47', 'PAMP', '80,300', '0,37%', '80,000', '1.769', '80,150', '80,450', '9.772', '877K', '$71.136K', '350', '13:54:55', 'SUPV', '60,000', '-', '60,000', '25.000', '59,500', '60,000', '2.000', '209K', '$12.496K', '196', '13:54:53', 'TECO2', '226,750', '-2,43%', '232,400', '41', '226,700', '227,000', '75', '31K', '$6.971K', '277', '13:53:54', 'TGNO4', '44,050', '0,11%', '44,000', '12.329', '44,000', '44,100', '27', '50K', '$2.199K', '85', '13:54:15', 'TGSU2', '165,500', '-0,27%', '165,950', '85', '164,500', '166,000', '10.000', '79K', '$13.124K', '109', '13:53:44', 'TRAN', '30,200', '0,16%', '30,150', '1.537', '30,050', '30,200', '8.192', '40K', '$1.212K', '146', '13:54:56', 'TXAR', '54,000', '1,31%', '53,300', '319', '53,800', '54,000', '17.364', '257K', '$13.718K', '209', '13:54:37', 'VALO', '27,500', '-1,07%', '27,800', '13.214', '27,500', '27,550', '1.190', '56K', '$1.546K', '100', '13:55:00', 'YPFD', '786,300', '1,89%', '771,650', '5', '785,500', '786,300', '30', '85K', '$66.943K', '611', '13:54:49']";

	}

	// Comienzo del analisis de la muestra
	
	$vaca = str_replace("[", "", $vaca);

	$vaca = substr($vaca, count($vaca), -1);

	$data = explode("', '", $vaca);

	$bloqueo = 11; // cantidad de items
	$fila = 0; // fila actual

	// recorremos todos los items
	for ($i=0; $i < count($data); $i++) { 
		
		// guardo en la fila actual lo que hay en 
		// data en esa columna

		$buffer[$fila][]= $data[$i];

		// si alcance las columnas de bloqueo
		if($i==$bloqueo){

			// incremento en 12 el bloqueo
			$bloqueo += 12;

			// agrego una fila nueva		
			$fila++;
		}		
	}

	// guardamos una muestra procesada en forma de matriz
	$columna1 = "<pre>".print_r($buffer, true)."</pre>";


	// preparamos la query para insertar
	$sql = "INSERT INTO `panelgeneral` (`campo1`, `campo2`, `campo3`, `campo4`, `campo5`, `campo6`, `campo7`, `campo8`, `campo9`, `campo10`, `campo11`, `campo12`) VALUES";

	// hacemos un recorrido por filas
	for ($i=0; $i < count($buffer); $i++) {

		// comienza una fila a insertar 
		$sql .= "(";
	
		// hacemos un recorrido por columnas
		for ($r=0; $r < count($buffer[$i]); $r++) {

			// si no es la primer columna ponemos una coma
			if($r!=0)
				$sql .= ",";

			// acumulamos el contenido de la columna
			$sql .= " '". $buffer[$i][$r] . "'";

		}

		// fin de la fila a insertar
		$sql .= '),';

	}

	// ya tenemos todas las filas listas asi que eliminamos la coma final
	$sql = substr($sql, 0, -3). ")";

	// guardamos una muestra procesada en forma que query
	$columna2 = $sql;

	// si existe base envia a la base de datos
	if(isset($_GET["base"])){

		$db = new mysqli(HOST, USER, PASS, DB);
		
		// Revisamos si en la base existe la tabla
		$res = $db->query("SHOW TABLES LIKE 'panelgeneral'");
			
		// Si la tabla no existe entonces la creamos
		if($res->num_rows == 0){
			$res = $db->query("CREATE TABLE `panelgeneral` (
  		 	`campo1` text NOT NULL,
	 		 `campo2` text NOT NULL,
 			 `campo3` text NOT NULL,
 			 `campo4` text NOT NULL,
			 `campo5` text NOT NULL,
			 `campo6` text NOT NULL,
			 `campo7` text NOT NULL,
			 `campo8` text NOT NULL,
			 `campo9` text NOT NULL,
			 `campo10` text NOT NULL,
			 `campo11` text NOT NULL,
			 `campo12` text NOT NULL
			  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
		}
		
		// Ejecutamos la query acumulada
		$res = $db->query($sql);

		echo $db->error ." ". $db->errno."<br>";

		$db->close();
	}

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
 	<title>CatchPanel</title>
 </head>
 <body>
 	
 	Muestra original:
 	<div style="width: 100%;">
 	<pre style="  white-space: pre-wrap;"><code class="html">
 		<?php echo $vaca;  ?>

 	</code></pre>
	</div>

 	<table border="1">
 		<tr>
 			<td>Matriz al aplicar<br> explode("', '")</td>
 			<td>Querys</td>
 		</tr>
 		<tr>
 			<td> <?php  echo $columna1; ?> </td>
 			<td style="vertical-align: top;"> <?php  echo $columna2; ?> </td>
 		</tr>
 	</table>
 	
 </body>
 </html>
